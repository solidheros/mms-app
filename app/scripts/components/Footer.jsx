import React from 'react';

const Footer = () => (
  <footer className="app__footer">
    <div className="app__container">
      <div className="app__footer__github">
        <h1>Im a footer</h1>
      </div>
    </div>
  </footer>
);

export default Footer;
